#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile.h"

int main(int argc, char *argv[]){
    char name[MAXSIZE],lastname[MAXSIZE];
    FILE *fp;
    char *fname = "";
     if (argc != 2){
        printf("Usage: ./a.out small.txt\n");
        exit(1);
    }
    fname = argv[1];

    
    struct Employee employee[MAXSIZE];
    int option;
    int six_digit_ID;//ID∫≈
    int success;
    int high;
    int salary;
    int define;

    int emp_num=0,list[MAXARR];
    if (open_file(fname)==-1){
        printf("error reading file\n");
        return -1;
    }
    fp=fopen(fname,"r");
    while(!feof(fp)){
        fscanf(fp,"%s  %s  %d  %d \n",employee[emp_num].name,employee[emp_num].lastname,&employee[emp_num].salary,&employee[emp_num].id);
        list[emp_num]=employee[emp_num].id;
        emp_num++;
    }
    fclose(fp);
    
    /*
        build a menu
        supply some choice ,each choice has its own function
        loop the menu, unless it choice quit function in menu.
    */

    int salary_number;//Print the number of the top salary rankings

    while(option!=10){
        printf("\nEmployee DB Menu:\n");
        printf("********************************************************\n");
        printf("(1) Print the Database\n");
        printf("(2) Lookup by ID\n");
        printf("(3) Lookup by Last Name\n");
        printf("(4) Add an Employee\n");
        printf("(5) Quit\n");
        printf("(6) Remove an employee\n");
        printf("(7) Update an employee's information\n");
        printf("(8) Print the M employees with the highest salaries\n");
        printf("(9) Find all employees with matching last name\n");
        printf("**********************\n");
        printf("Enter your choice: ");
        read_int(&option);
        //pemp=employee;
        qsort(employee,emp_num,sizeof(employee[0]),comp);
        //id_sort(pemp,emp_num);
            for(int i=0;i<emp_num;i++){
             list[i]=employee[i].id;
            }

        switch(option){

        case 1:
            printf("********************************************************\n");
            printf(" Name                     SALARY       ID\n");
            printf("********************************************************\n");
            int i=0;
            while(i!=emp_num){
                printf("%-10s %-10s %10d %10d \n",employee[i].name,employee[i].lastname,employee[i].salary,employee[i].id);
                i++;
            }
            printf("*********************************************************\n");
            printf("Number of Employees (%d)\n",emp_num);
            break;
        case 2:
            printf("Enter a 6 digit employee id: ");
            read_int(&six_digit_ID);
            success =binary_search(list,0,emp_num,six_digit_ID);
            if(success==-1){
                printf("Employee with id %d not found in DB\n",six_digit_ID);
            }else{
                print_by_success(employee,success);
            }
            break;


        case 3:
            printf("Enter Employee's last name (no extra spaces): ");
            read_string(lastname);
            success = search_lastname(employee,emp_num,lastname);
            if(success==-1){
                printf("Employee with lastname %s not found in DB\n",lastname);
            }else{
                print_by_success(employee,success);
            }
            break;
        case 4:
            printf("Enter the first name of the employee: ");
            read_string(name);
            printf("Enter the last name of the employee: ");
            read_string(lastname);
            printf("%s",lastname);
            printf("Enter employee's salary ($30,000 and $150,000): ");
            read_int(&salary);
            printf("do you want to add the following employee to the DB?\n");
            printf("%s %s , salary: %d\n",name,lastname,salary);
            printf("Enter 1 for yes, 0 for no: ");
            read_int(&define);
            if(define==1){
                if(salary>=30000&&salary<=150000){
                    int now_id=employee[emp_num-1].id;
                    int new_id=now_id+1;
                    if(new_id<100000||new_id>999999){
                        printf("id number is out of bound\n");
                        break;
                    }
                    strcpy(employee[emp_num].name,name);
                    strcpy(employee[emp_num].lastname,lastname);
                    employee[emp_num].salary=salary;
                    employee[emp_num].id=new_id;
                    list[emp_num]=employee[emp_num].id;
                    emp_num++;
                    printf("adding successfully.\n");
                }else{
                    printf("salary invalid, please enter again.\n");
                }
            }
            break;

        case 5:
            printf("goodbye!\n");
            exit(0);
            break;


        case 6:
            printf("Enter a 6 digit employee id: ");
            read_int(&six_digit_ID);
            success=binary_search(list,0,emp_num,six_digit_ID);
            if(success==-1){
                printf("This ID %d not exist in DB",six_digit_ID);
            }else{
                printf("Are you sure to delete the user？1 yes，0 no");
                read_int(&define);
                if(define==1){
                    //delete
                    for(int i=success;i<emp_num;i++){
                        employee[success]=employee[success+1];
                    }
                    emp_num=emp_num-1;
                    save_file(fname, emp_num, employee);
                    printf("delete success.");
                }else{
                    printf("fail to delete~");
                }
            }
            break;

        case 7: //update 

                printf("Enter a six digit employee id:\n");
                 int num;
                int update_id;
                scanf("%d",&update_id);
                int success1 = binary_search(list,0,emp_num,update_id);
                if(success1==-1){
                    printf("ID %d is not found \n",update_id);
                }else{
                    int item;
                    while(1){
                        printf("------------------\n");
                        printf("1.update first name\n");
                        printf("2.update last name\n");
                        printf("3.update salary\n");
                        printf("4.exit update\n");
                        printf("------------------\n");
                        printf("Please choose the NO. of the item:");
                        scanf("%d",&item);
                        if(item==1){
                            printf("please enter the new first name:\n");
                            scanf("%s",name);
                            strcpy(employee[success1].name,name);
                        }
                        if(item==2){
                            printf("please enter the new last name:\n");
                            scanf("%s",lastname);
                            strcpy(employee[success1].lastname,lastname);
                        }
                        if(item==3){
                            printf("Plesea int salary:\n");
                            scanf("%d",&salary);
                            employee[success1].salary=salary;
                        }
                        if(item==4){
                            printf("Exit……");
                            break;
                        }
                    }
                    save_file(fname, emp_num, employee);
                    printf("The update is complete! Please save them in time！\n");
                }
                break;
            case 8:
                printf("Enter the number with the highest salaries: ");
                read_int(&salary_number);
                while(1){
                    if(salary_number<0||salary_number>emp_num){
                        printf("M is outBound,Please input again:");
                        read_int(&salary_number);
                    }else{
                        break;
                    }
                }
                struct Employee *employee_temp=employee;
                qsort(employee_temp,emp_num,sizeof(employee_temp[0]),salary_sort);
                printf("*********************************************************\n");
                printf("firstname    lastname        salary       id\n");
                int n=0;
                while(n<salary_number){
                    printf("%-10s %-10s %10d %10d \n",employee_temp[emp_num-1-n].name,employee_temp[emp_num-1-n].lastname,employee_temp[emp_num-1-n].salary,employee_temp[emp_num-1-n].id);
                    n++;
                }
                printf("*********************************************************\n");
                break;
            case 9:
                printf("Enter Employee's last name (no extra spaces): ");
                read_string(lastname);
                success = search_lastname_1(employee,emp_num,lastname);
                if(success == -1){
                    printf("Employee with lastname %s not found in DB\n",lastname);
                }else{
                    printf("firstname      lastname       ID     salary\n");
                    for (int i =0; i < emp_num; i++) {
                        if(strcasecmp(employee[i].lastname, lastname) ==0){
                            printf("%-10s %-16s %-16d %d\n",employee[i].name, employee[i].lastname,employee[i].id,employee[i].salary);
                        }
                    }
                    
                }
                break;
            default:
                printf("Please input the number of 1-9\n");
                break;
        }
    }
}

