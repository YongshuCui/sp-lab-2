# Employee Management System#


### Bitbucket Link ###


* [Link](https://YongshuCui@bitbucket.org/YongshuCui/sp-lab-2.git)

### This system implement an employee database program in C programming. And there are four steps below ###

* First, You need to find the employee’s ID in the DB before Remove an employee，and then confirm if you want to delete the employee's information?
* Second，please Enter the employee ID before updating the employee information, and then confirm that you want to update his information?
* Third, the first thing to do is to determine the scope of M If the M < 0 or M > employees number, prompt: M crosses the line, please re-enter M. You can judge by the while loop. The next step is to redefine a structure that is temporary. (struct Employee *temp =employee; Employee is a structure that I have previously declared myself.) Using fast-row qsort (temp, number of employees, sizeof (temp, compare_salary) ); Sort by employee's salary. And then print the information.
* In the last, Find all employees with matching last name.Enter a last name manually first.Then start looking for last name in the database, and then match all the names of this last name. Print them in the last.


### This program contains 4 important features ###

* Print the Database
* Lookup employee by ID
* Delete an Employee
* Update an Employee



### Step to run ###

* Open the program directory, The project contains the following file structure:

```
.
├── main.c             // main program 
├── readfile.h         // library about read file, read int, read char, read float 
├── README.md          // program documentation
├── small.txt          // db file
└── a.out              // execute program
```

* Execute program enter small.txt
