#define MAXSIZE 64
#define MAXARR 1024

struct Employee {
    char name[MAXSIZE];
    char lastname[MAXSIZE];
    int  id;
    int salary;
};

int open_file(char *fname){
    if(fopen(fname, "r") == NULL){
        return -1;
    }

    return 0;
}

int read_int(int *variable){
    if(scanf("%d", variable) == 0){
        return -1;
    }
    return 0;
}

int read_float(float *variable){
    if(scanf("%f", variable) == 0){
        return -1;
    }
    return 0;
}

int read_string(char *variable){
    if(scanf("%s", variable) == 0){
        return -1;
    }

    return 0;
}

void close_file(FILE *fname){
    fclose(fname);
}
/*
compare specified variables,
it would be used in sort a list.
*/
int comp(const void *p1,const void *p2){
    int id1=((struct Employee *)p1)->id;
    int id2=((struct Employee *)p2)->id;
    return id1>id2?1:-1;
}

int binary_search(const int arr[],int min,int max,int target){
    if(max>=min){
        int mid=min+(max-min)/2;
        if(arr[mid]==target){
            return mid;
        }else if(arr[mid]>target){
            return binary_search(arr,min,mid-1,target);
        }else{
            return binary_search(arr,mid+1,max,target);
        }
    }
    return -1;
}

void print_by_success(struct Employee employee[MAXSIZE],int key){
    printf("*********************************************************\n");
    printf("Name                       SALARY    ID\n");
    printf("%-10s %-10s %10d %10d \n",employee[key].name,employee[key].lastname,employee[key].salary,employee[key].id);
    printf("*********************************************************\n");
}

int search_lastname(struct Employee employee[MAXSIZE],int emp_num,char lastname[]){
    int i=0;
    while(i!=emp_num){
        if(strcmp(employee[i].lastname,lastname)==0){
            return i;
        }
        i++;
    }
    return -1;
}

int search_lastname_1(struct Employee employee[MAXSIZE],int emp_num,char lastname[]){
    int i=0;
    while(i!=emp_num){
        if(strcasecmp(employee[i].lastname,lastname)==0){
            return i;
        }
        i++;
    }
    return -1;
}


void del (struct Employee employee[MAXSIZE], int id){
    int i = 0;
    printf(" if s/he really wants to remove\n");
    printf(" please enter the id of  employee:\n");
    scanf("%d",&id);

}

void update (struct Employee employee[MAXSIZE],int key){
    printf("*********************************************************\n");
    printf("Name                       SALARY    ID\n");
    printf("%-10s %-10s %10d %10d \n",employee[key].name,employee[key].lastname,employee[key].salary,employee[key].id);
    printf("*********************************************************\n");
}


void save_file(char *fname, int emp_num, struct Employee *employee){
    FILE *fp;
    if((fp = fopen(fname, "w")) == NULL){
        printf("Can't open the %s", fname);
    }else{
        for(int i=0; i < emp_num; i++){
            fprintf(fp, "%-10s\t%-10s\t%d\t%d\n", employee[i].name, employee[i].lastname, employee[i].salary, employee[i].id);
        }
    }
}


int salary_sort(const void *p1,const void *p2){
    int id1=((struct Employee *)p1)->salary;
    int id2=((struct Employee *)p2)->salary;
    return id1>id2?1:-1;
}

